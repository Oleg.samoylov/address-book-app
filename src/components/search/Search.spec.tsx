import 'jsdom-global/register';
import * as React from "react";
import {Search} from "./Search";
import {mount, shallow} from "enzyme";

describe('<Search/>', () => {
    it('Component should be truthy', () => {
        const search = shallow(<Search onQueryChange={()=>{}} />);
        expect(search).toBeTruthy();
    });
    it('Callback should be triggered with correct value', () => {
        let value;
        const callback = jest.fn();
        const search = mount(<Search onQueryChange={callback} />);
        search.find('input').simulate('change', { target: { value: "test"}});
        expect(callback).toBeCalledWith('test');
    });

});