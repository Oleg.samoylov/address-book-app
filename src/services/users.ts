import axios from 'axios';
import {IUserRequestParams} from "../store/users/types";

export class UserService {
    static getUsers(params: IUserRequestParams): Promise<any> {
        return axios.get('https://randomuser.me/api/', { params })
    }
}