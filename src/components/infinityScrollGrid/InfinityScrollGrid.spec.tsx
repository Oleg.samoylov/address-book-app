import 'jsdom-global/register';
import {mount, shallow} from "enzyme";
import {InfinityScrollGrid} from "./InfinityScrollGrid";
import * as React from "react";

describe('<InfinityScrollGrid/>', () => {
    it('Component should be truthy', () => {
        const search = shallow(<InfinityScrollGrid
            loading={false}
            component={(i: number)=> <div>{i}</div>}
            listEndComponent={()=> <div>test</div>}
            loaded={false}
            onLoad={()=>{}}
            list={[1,2,3]} />);
        expect(search).toBeTruthy();
    });
    it('Callback for load should be trigger', () => {
        const callback = jest.fn(()=>{});
        const component = ()=> (<div>test</div>);
        const comp = mount(<InfinityScrollGrid
            loading={false}
            component={component}
            listEndComponent={()=> <p>test</p>}
            loaded={false}
            onLoad={callback}
            list={[1,2,3]} />);
        const event = { target: { scrollTop: 10, clientHeight: 10, scrollHeight: 20}};
        comp.find('#infinity-scroll-grid').simulate('scroll', event);
        expect(callback).toHaveBeenCalled();
    });

});