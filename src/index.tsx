import * as React from "react";
import * as ReactDOM from "react-dom";
import {Route, Switch, HashRouter, NavLink} from 'react-router-dom';
import HomePage from "./pages/home/Home";
import SettingsPage from "./pages/settings/Settings";
import {AppStore, configureStore} from "./store";
import {Provider} from "react-redux";
import {AppBar, Toolbar} from "@material-ui/core";
import SettingsIcon from "@material-ui/icons/Settings";
import HomeIcon from "@material-ui/icons/Home";

const store: AppStore = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <AppBar position="static" color="default">
                <Toolbar>
                    <NavLink to={'/'}><HomeIcon/></NavLink>
                    <NavLink to={'settings'}><SettingsIcon/></NavLink>
                </Toolbar>
            </AppBar>
            <Switch>
                <Route path="/" exact component={HomePage}/>
                <Route path="/settings" exact component={SettingsPage}/>
            </Switch>
        </HashRouter>
    </Provider>,
    document.getElementById("app")
);