import * as React from "react";
import {User} from "../../store/users/types";
import {
    Avatar, Button,
    Card, CardActions,
    CardContent,
    CardHeader, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    makeStyles, TextField,
    Typography
} from "@material-ui/core";
import {useState} from "react";

const useStyles = makeStyles(() => ({
    cardHeader: {
        textTransform: 'capitalize',
    },
    form: {
        display: 'flex',
        flexDirection: 'column'
    },
    image: {
        width: '100%'
    }
}));

const UserCard: React.FC<User> = (props: User): React.ReactElement => {
    const classes = useStyles({});
    const [open, setOpen] = useState(false);
    return (
        <>
            <Card>
                <CardHeader
                    avatar={
                        <Avatar src={props.picture.thumbnail}>
                        </Avatar>
                    }
                    title={props.name.last}
                    subheader={props.name.first}
                    className={classes.cardHeader}
                />
                <CardContent>
                    <Typography variant="body2" color="textSecondary">
                        {props.email}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" color="primary" onClick={()=> setOpen(true)}>
                        Show more
                    </Button>
                </CardActions>
            </Card>
            <Dialog open={open} onClose={()=> setOpen(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Details</DialogTitle>
                <DialogContent>
                    <img src={props.picture.large} className={classes.image}/>
                    <form className={classes.form}>
                    <TextField
                        label="Firstname"
                        value={props.name.first}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    <TextField
                        label="Lastname"
                        value={props.name.last}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    <TextField
                        label="Email"
                        value={props.email}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    <TextField
                        label="Street"
                        value={props.location.street}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    <TextField
                        label="City"
                        value={props.location.city}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    <TextField
                        label="State"
                        value={props.location.state}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    <TextField
                        label="Postcode"
                        value={props.location.postcode}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    <TextField
                        label="Phone"
                        value={props.phone}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    <TextField
                        label="Cell"
                        value={props.cell}
                        margin="dense"
                        variant="outlined"
                        disabled={true}
                    />
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=> setOpen(false)} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    )
};

export default UserCard;
