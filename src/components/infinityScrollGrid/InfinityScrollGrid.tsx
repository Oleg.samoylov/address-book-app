import * as React from "react";
import {Fade, Grid, makeStyles} from "@material-ui/core";

interface IScrollGridProps {
    list: any[],
    component: any,
    onLoad: () => void,
    loading: boolean,
    loaded: boolean,
    listEndComponent: () => React.ReactElement
}

const useStyles = makeStyles(() => ({
    scrollable: {
        overflowY: 'auto',
        maxHeight: 'calc(95vh - 154px);',
        padding: '20px 20px 50px',
        position: 'relative'
    },
    loader: {
        position: 'fixed',
        background: 'transparent',
        left: '0',
        width: '100%',
        top: '15%',
        color: 'green',
        textAlign: 'center',
        zIndex: 1
    }
}));


export const InfinityScrollGrid: React.FC<IScrollGridProps> = (props: IScrollGridProps): React.ReactElement => {
    const {list, component, onLoad, loading, loaded, listEndComponent} = props;
    const classes = useStyles({});
    const scrollHandler = (ev: any) => {
        if (ev.target.scrollHeight === ev.target.scrollTop + ev.target.clientHeight) {
            onLoad();
        }
    };

    return (
        <div id="infinity-scroll-grid" className={classes.scrollable} onScroll={scrollHandler}>
            <Fade in={loading}>
                <div className={classes.loader}>Loading...</div>
            </Fade>
            <Grid container spacing={1}>
                {list.map((data: any, i) => {
                    return <Grid item xs={12} sm={6} lg={4} key={i}>
                        {React.createElement(component, data)}
                    </Grid>
                })
                }
            </Grid>
            {loaded && listEndComponent()}
        </div>
    )
};