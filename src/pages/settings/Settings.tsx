import * as React from "react";
import {Checkbox, FormControlLabel, FormGroup, makeStyles, Typography} from "@material-ui/core";
import {AppState} from "../../store";
import {Dispatch} from "redux";
import {setNationality} from "../../store/users/actions";
import {connect} from "react-redux";
import {ChangeEvent} from "react";

const availableNationalities: string[] = ['CH', 'ES', 'FR', 'GB'];

const useStyles = makeStyles(() => ({
    formGroup: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    }
}));

interface ISettingsProps {
    setNat: (key: string, value: boolean) => void,
    nationalities: string
}

const Settings: React.FC<ISettingsProps> = (props: ISettingsProps): React.ReactElement => {
    const classes = useStyles({});
    const onChangeHandler = (ev: ChangeEvent<HTMLInputElement>) => {
        props.setNat(ev.target.value, ev.target.checked);
    };
    return (
        <>
            <Typography align="center" variant="h3">Choose nationalities</Typography>
            <FormGroup row className={classes.formGroup}>
            {availableNationalities.map((nationality)=> {
                return (
                    <FormControlLabel
                        key={nationality}
                        control={<Checkbox
                            value={nationality}
                            checked={props.nationalities.includes(nationality)}
                            onChange={onChangeHandler} />}
                        label={nationality} />
                )
            })}
            </FormGroup>
        </>
    )
};

const mapStateToProps = (store: AppState) => ({
    nationalities: store.users.params.nat,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
    setNat: (key: string, value: boolean) => {
        dispatch(setNationality(key, value));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);