import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';
import {applyMiddleware, combineReducers, createStore, Store} from "redux";
import { usersReducer } from "./users/reducer";

const rootReducer = combineReducers({
    users: usersReducer
});

export type AppState = ReturnType<typeof rootReducer>;
export type AppStore = Store<AppState>;

export const configureStore = (): Store => {
    return createStore(
        rootReducer,
        applyMiddleware(...[thunkMiddleware, loggerMiddleware])
    )
};