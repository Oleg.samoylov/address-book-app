import {Action} from "redux";

interface IName {
    title: string;
    first: string;
    last: string;
}

interface ILocation {
    street: string;
    city: string;
    state: string;
    postcode: string;
}

interface IPicture {
    thumbnail: string;
    large: string;
}

export interface User {
    picture: IPicture;
    name: IName;
    email: string;
    location: ILocation;
    phone: string;
    cell: string;
}

export enum UserActionType {
    LOADING = 'loading_users',
    LOADED = 'loaded_users',
    RESET_STATE = 'reset',
    SET_NATIONALITY = 'set_nationality'
}

export interface IUserRequestParams {
    page: number,
    results: number,
    nat?: string
}

export interface IUserAction extends Action {
    list?: User[],
    params?: IUserRequestParams,
    nationality?: string
}


export interface IUsersState {
    tmpList: User[];
    list: User[];
    params: IUserRequestParams,
    catalogLoaded: boolean,
    loading: boolean,
}